const fs = require('fs');
const http = require('http');
const event_emitter = require('events');
const formidable =  require('formidable')

// var read_stream = fs.createReadStream('text.txt');
// read_stream.on('open', () => {
//    console.log('File is opened');
// })

// var emitter = new event_emitter.EventEmitter()
// var time = 0;
// var announce_time = function(){
//    var interval = setInterval(()=> {
//       console.log('The current time is ' + time);
//       if(time > 10){
//          console.log('The time exceeds 10 secs ago');
//          return clearInterval(interval);
//       }
//       time +=2;
//    }, 2000, )
// }

// emitter.on('past_time', announce_time)
// emitter.emit('past_time');

http.createServer((req, res) => {
   var urlRes = req.url;
   if(urlRes == '/fileupload'){
      var form = new formidable.IncomingForm();
      form.parse(req, (err, fields, files)=> {
         var oldpath = files.filetoupload.path;
         var newpath = __dirname + '/'+ files.filetoupload.name;

         fs.rename(oldpath, newpath, (err) =>{
            if (err){
               return console.log('Path can not be renamed');
            }
         })

         res.write('File is uploaded');
         return res.end();
      })
   }
   else{
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.write('<form action="fileupload" method="post" enctype="multipart/form-data">');
      res.write('<input type="file" name="filetoupload"><br>');
      res.write('<input type="submit">');
      res.write('</form>');
      res.end();
   }
  
 }).listen(3000);




