const url = require('url');
const fs = require('fs');
const http = require('http');

// var add = 'http://localhost:8080/default.htm?year=2019&month=January';

// var queryResult = url.parse(add, true);
// console.log('The year is ' + queryResult.query.year + ' and The month is ' + queryResult.query.month);
// console.log('The host name is ' + queryResult.host);
// console.log('The search is ' + queryResult.search);
// console.log('The path name is ' + queryResult.pathname);

http.createServer((req, res) => {
   var responseUrl = req.url;
   if(responseUrl == '/summer'){
      fs.readFile('summer.html', 'utf8', (err, data) => {
         if (err) {
            res.writeHead(404, {'Content-Type': 'text/html'});
            return res.end("404 Not Found");
          }  
          res.writeHead(200, {'Content-Type': 'text/html'});
          res.write(data, (err) =>{
            if(err){
              return console.log('Error in loading the html file')
            }
         });
          return res.end();
      })
   }
   if(responseUrl == '/winter'){
      fs.readFile('winter.html', 'utf8', (err, data) => {
         if (err) {
            res.writeHead(404, {'Content-Type': 'text/html'});
            return res.end("404 Not Found");
          }  
          res.writeHead(200, {'Content-Type': 'text/html'});
          res.write(data, (err) =>{
             if(err){
               return console.log('Error in loading the html file')
             }
          });
          return res.end();
      })
   }
   if(responseUrl == '/') {
      res.write('Change the URL of this site')
      res.end();
   }
}).listen(4000);