const fileSystem = require('fs');

requestHandler = function(request, response){
   const url = request.url;
   const method = request.method;
   if(url === '/'){
      response.write('<html>');
      response.write('<head><title>Enter Message</title></head>');
      response.write('<body><form method="POST" action="/message"><label>Password</label><input type="text" name="message" /><br /><button type="submit">Submit</button></form></body>');
      response.write('</html>');
      return response.end();
   }

   if(url === '/message' && method === "POST"){
      const data = [];
      request.on('data', (chunk) =>{
         data.push(chunk);
      });

      return request.on('end', () => {
         const parsedBody = Buffer.concat(data).toString();
         const message = parsedBody.split('=')[1];
         fileSystem.writeFile('message.txt', message, err => {
            response.statusCode = 302;
            response.setHeader('Location', '/welcome');
            return response.end()
         });
      })
   }

   if(url === '/welcome'){
      response.setHeader('Content-Type', 'text/html');
      response.write('<head><title>My first Page</title></head>');
      response.write('<body><h1><b>Hello from my Node Development</b></h1></body>');
      response.write('</html>');
      response.end();
   }
}

module.export = requestHandler;