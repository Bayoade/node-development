const nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
   service: 'gmail',
   auth: {
     user: 'youremail@gmail.com',
     pass: 'mypassword'
   }
 });
 
 var mailOptions = {
   from: 'myemail@gmail.com',
   to: 'friendemail@yahoo.com, otherfriendemail@gmail.com',
   subject: 'Sending Email using Node.js',
   text: 'That was easy!'
 };

 transporter.sendMail(mailOptions, (err, info) => {
    if(err){
       return console.log('Mail could not be sent');
    }
    console.log('Email response ' + info.response);
 })