// const sayHello = require('./examples')

// console.log('Greetings in English ' + sayHello.sayHelloInEnglish());
// console.log('Greetings in Yoruba ' + sayHello.sayHelloInYoruba());

const file = require('fs');

// var read_file = file.readFileSync('text.txt', 'utf8');
// console.log(read_file);

// file.writeFileSync('text2.txt', read_file);

// var read_file = file.readFile('text.txt', 'utf8', (err, data) => {
//    if(err){
//       throw err;
//    }

//    console.log(data);
//    return data;
// });

// file.writeFile('text3.txt', read_file, 'utf8', (err) => {
//    if(err){
//       throw err;
//    }
//    console.log('Success!!!');
// })

// file.appendFile('text.txt', ' I have just being appended', (err)=>{
//    if(err){
//       return console.log('failed to append')
//    }
//    console.log('Appended!!')
// })

// file.open('newFile.txt', 'w', (err, doc) => {
//    if(err){
//       return console.log('it wasnt created');
//    }
//    console.log('Created!!');
//    file.writeFile(doc, 'I am the content', (err) =>{
//       console.log('File content has been written');
//       return console.log('All done!!!');
//    })
// })

// file.writeFile('newFile1.txt', 'I am also a content here', (err)  =>{
//    if(err){
//       return console.log('No file is created');
//    }
//    console.log('New file created and content written');
// })

// file.exists('newFile1.txt', (check) => {
//    if(check){
//       file.unlink('newFile1.txt', (err) => {
//          if(err){
//             return console.log('File not available');
//          }
//          console.log('File has been permanently deleted');
//       })
//    }
// })

file.rename('examples.js', 'moduleexports.js', (err) =>{
   if(err){
      return console.log('file is not found');
   }

   return console.log('File has been renamed!!');
})