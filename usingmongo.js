const mongoClient = require('mongodb').MongoClient;

var url = "mongodb://localhost:27017/";

mongoClient.connect(url, (err, db) => {
   if(err){
      return console.log('Database connection is not established')
   }
   var dbo =  db.db('mycreateddb');

   // dbo.createCollection('userProfiles', (err, data) => {
   //    if(err){
   //       return console.log('Could not create user profile table');
   //    }
   //    console.log('Created userProfile table');
   //    db.close();
   // })

   // dbo.collection('userProfiles').insertOne(
   //    { Name: 'Adebayomi FARINDE', University: 'OAU'},
   //    (err, info) => {
   //       if(err){
   //          return console.log('No user is inserted')
   //       }

   //       console.log('Added one user profile in the database');
   //       db.close();
   //    })

   // dbo.collection('userProfiles').find({}, { projection: { _id: 0, Name: 1, University: 1 } }).toArray((err, result) => {
   //    if(err){
   //       return console.log('Error while running the query');
   //    }
   //    console.log(result);
   //    db.close();
   // })

   // dbo.collection('userProfiles').find(
   //       { Name: 'Adebayomi FARINDE'},
   //       { projection: { _id: 0, Name: 1, Address: 1}}).toArray((err, data)=> {
   //          if(err){
   //             return console.log('Error while accessing the db');
   //          }
   //          console.log(data);
   //    })
   // var query = { Name: '/^A/'};
   // // ascending is 1 while descending is -1
   // var sort = { Name: 1};
   // dbo.collection('userProfiles').find(query).sort(sort).toArray((err, data)=> {
   //    if(err){
   //       return console.log('Error while accessing the db');
   //    }
   //    console.log(data);
   // })

   // var newQuery = { Name: 'Adebayomi FARINDE' };
   // //newQuery = { Name: '/^A/' };
   // dbo.collection('userProfiles').deleteMany(newQuery, (err, res) => {
   //    if(err){
   //       return console.log('Unable to delete the item in the db');
   //    }
   //    console.log(res.result.n + ' item has been deleted');
   // })

   // dbo.collection('userProfile').drop((err, delOk)=> {
   //    if(err){
   //       return console.log('Collection was not deleted')
   //    }

   //    console.log('Collection has been deleted');
   // })

   dbo.collection('userProfiles').updateOne({ Name: 'Adebayomi FARINDE'},
      { $set : { Name: 'Matthew Farinde', University: 'UNILAG'}},
      (err, res) =>{
         if(err){
            return console.log('Can not update the database');
         }

         console.log('Object has been updated');
         db.close();
      }
   )
})